package kata.bingoCard;

import java.util.*;

public class BingoCard {

    private static int length = 24;

    public static String[] getCard() {
        List<String> list = new ArrayList<>(length);

        list.addAll(draw("B", 5, 1, 15));
        list.addAll(draw("I", 5, 16, 30));
        list.addAll(draw("N", 4, 31, 45));
        list.addAll(draw("G", 5, 46, 60));
        list.addAll(draw("O", 5, 61, 75));

        return list.toArray(new String[list.size()]);
    }

    private static Set<String> draw(String style, int size, int lowerBound, int upperBound) {
        Set<String> sets = new HashSet<>(upperBound);
        while (sets.size() < size) {
            sets.add(style + String.valueOf(
                    new Random().ints(lowerBound, upperBound+1).findFirst().getAsInt()
                    ));
        }
        return sets;
    }
}
