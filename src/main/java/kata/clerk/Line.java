package kata.clerk;

/**
 * Created by johnson on 2016/5/26.
 */
public class Line {

    public static String tickets(int[] bills) {
        int bill25 = 0;
        int bill50 = 0;

        String result = "YES";
        for (int i = 0; i < bills.length; i++) {
            int indesk = bills[i];
            if (indesk == 50) {
                if (bill25 >= 1) {
                    bill25--;
                    bill50++;
                } else {
                    result = "NO";
                    break;
                }
            } else if (indesk == 25) {
                bill25++;
            } else if (indesk == 100) {
                if (bill25 >= 3) {
                    bill25 -= 3;
                } else if (bill25 >= 1 && bill50 >= 1) {
                    bill25--;
                    bill50--;
                } else {
                    result = "NO";
                    break;
                }
            } else {
                throw new IllegalArgumentException("only accept 25, 50 or 100 dollars bill~");
            }
        }
        return result;
    }
}
