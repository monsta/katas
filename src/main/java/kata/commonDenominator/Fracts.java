package kata.commonDenominator;

/**
 * Created by johnson on 2016/6/22.
 */
public class Fracts {
    public static String convertFrac(long[][] lst) {
        if (lst == null || lst.length == 0) {
            return "0";
        }
        long[] dividends = new long[lst.length];
        for (int i = 0; i < lst.length; i++) {
            dividends[i] = lst[i][1];
        }
        long d = findLCM(dividends);

        StringBuffer rtn = new StringBuffer();
        for (int i = 0; i < lst.length; i++) {
            rtn.append("(").append(
            lst[i][0] * (d / lst[i][1]))
                    .append(",")
                    .append(d)
                    .append(")");
        }
        return rtn.toString();
    }

    private static long findLCM(long[] nums) {
        long lcmTemp = nums[0];
        for (int i = 1; i < nums.length; i++) {
            lcmTemp = getLCM(lcmTemp, nums[i]);
        }
        return lcmTemp;
    }

    private static long getLCM(long num1, long num2) {
        long first;
        first = (num1 > num2) ? num1 : num2;
        while (true) {
            if (first % num1 == 0 && first % num2 == 0) {
                return first;
            }
            ++first;
        }
    }
}
