package kata.decodeMorseCode;

import java.util.*;

/**
 * Created by johnson on 2016/6/16.
 */
public class MorseCodeDecoder {

    public static String decode(String morseCode) {
        Map<String, String> morseCodeMap = initializeMorseCode();
        String[] codeWords = morseCode.split("   ");

        StringBuffer result = new StringBuffer();
        for (String codeWord : codeWords) {
            List<String> allMorseCodeLetter = new ArrayList<>();
            Collections.addAll(allMorseCodeLetter, codeWord.split("\\s+"));
            for (int i = 0; i < allMorseCodeLetter.size(); i++) {
                String decode = morseCodeMap.get(allMorseCodeLetter.get(i));
                result.append(decode==null?"":decode);
            }
            result.append(" ");
        }

        return result.toString().trim();
    }

    private static Map<String, String> initializeMorseCode() {
        Map<String, String> morseCode = new HashMap<>();
        morseCode.put(".-", "A");
        morseCode.put("-...", "B");
        morseCode.put("-.-.", "C");
        morseCode.put("-..", "D");
        morseCode.put(".", "E");
        morseCode.put("..-.", "F");
        morseCode.put("--.", "G");
        morseCode.put("....", "H");
        morseCode.put("..", "I");
        morseCode.put(".---", "J");
        morseCode.put("-.-", "K");
        morseCode.put(".-..", "L");
        morseCode.put("--", "M");
        morseCode.put("-.", "N");
        morseCode.put("---", "O");
        morseCode.put(".--.", "P");
        morseCode.put("--.-", "Q");
        morseCode.put(".-.", "R");
        morseCode.put("...", "S");
        morseCode.put("-", "T");
        morseCode.put("..-", "U");
        morseCode.put("...-", "V");
        morseCode.put(".--", "W");
        morseCode.put("-..-", "X");
        morseCode.put("-.--", "Y");
        morseCode.put("--..", "Z");
        morseCode.put("...---...", "SOS");

        morseCode.put(".----", "1");
        morseCode.put("..---", "2");
        morseCode.put("...--", "3");
        morseCode.put("....-", "4");
        morseCode.put(".....", "5");
        morseCode.put("-....", "6");
        morseCode.put("--...", "7");
        morseCode.put("---..", "8");
        morseCode.put("----.", "9");
        morseCode.put("-----", "0");
        morseCode.put(" ", " ");
        morseCode.put(".-.-.-", ".");
        morseCode.put("-.-.--", "!");

        return morseCode;
    }
}
