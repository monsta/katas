package kata.deltaBits;

/**
 * Created by johnson on 2016/6/23.
 */
public class DeltaBits {
    public static int convertBits(int a, int b) {
        return diff1BitCount(
            leftPad(Integer.toString(a, 2), 31, '0'),
            leftPad(Integer.toString(b, 2), 31, '0')
        );
    }

    private static int diff1BitCount(String binary1, String binary2) {
        char[] chars1 = binary1.toCharArray();
        char[] chars2 = binary2.toCharArray();

        int count = 0;
        for (int i = 0; i < chars1.length; i++) {
            if (chars1[i] != chars2[i]) {
                count++;
            }
        }
        return count;
    }

    private static String leftPad(String str, int padLen, char padhcar) {
        int pad = padLen - str.length();
        if (pad > 0) {
            char[] padCharArr = new char[pad];
            for (int i = 0; i < pad; ++i) {
                padCharArr[i] = padhcar;
            }
            return new String(padCharArr).concat(str);
        }
        return str;
    }
}
