package kata.deltaBits;

/**
 * Created by johnson on 2016/6/23.
 */
public class DeltaBitsXOR {
    public static int convertBits(int a, int b) {
        return Integer.bitCount(a ^ b);
    }
}
