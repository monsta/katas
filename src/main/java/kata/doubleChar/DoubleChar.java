package kata.doubleChar;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;

/**
 * Created by johnson on 2016/5/24.
 */
public class DoubleChar {
    public static String eval(String str) {
        char[] chars = str.toCharArray();
        char[] doubleChars = new char[chars.length * 2];

        for (int i = 0; i < chars.length; i++) {
            char aChar = chars[i];
            doubleChars[i*2] = aChar;
            doubleChars[i*2+1] = aChar;
        }

        return new String(doubleChars);
    }
}
