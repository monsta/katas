package kata.duplicateEncoder;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by johnson on 2016/5/31.
 */
public class DuplicateEncoder {

    static String encode(String original) {
        Map<Character, Integer> charCount = new HashMap<>();

        char[] chars = original.toCharArray();
        for (int i = 0; i < chars.length; i++) {
            char c = Character.toLowerCase(chars[i]);
            if (charCount.get(c) != null) {
                charCount.put(c, charCount.get(c) + 1);
            } else {
                charCount.put(c, 1);
            }
        }

        char moreThanOnce = ')';
        char onlyOnce = '(';
        for (int i = 0; i < chars.length; i++) {
            char c = Character.toLowerCase(chars[i]);
            if (charCount.get(c) > 1) {
                chars[i] = moreThanOnce;
            } else {
                chars[i] = onlyOnce;
            }
        }

        return new String(chars);
    }
}
