package kata.factorial;

/**
 * Created by johnson on 2016/5/25.
 */
public class Factorial {

    public int factorial(int i) {
        if (i == 0 || i == 1) {
            return 1;
        } else if (i < 0 || i > 12) {
            throw new IllegalArgumentException();
        } else {
            return i * factorial(i - 1);
        }
    }
}
