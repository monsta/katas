package kata.functionIteration;

import java.util.function.Function;

/**
 * Created by johnson on 2016/6/7.
 */
public class GetIterator {
    public static Function<Integer, Integer> getIterator(Function<Integer, Integer> function, int times) {
        return x -> {
            for (int i = 0; i < times; i++) {
                x = function.apply(x);
            }
            return x;
        };
    }
}
