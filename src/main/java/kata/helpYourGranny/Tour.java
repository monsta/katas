package kata.helpYourGranny;

import java.util.Map;

/**
 * Created by johnson on 2016/6/14.
 */
public class Tour {
    public static int tour(String[] arrFriends, String[][] ftwns, Map<String, Double> h) {
        String fromTown = "X0";
        h.put(fromTown, 0.0);

        double tour = 0;
        for (int i = 0; i < ftwns.length; i++) {
            String friendsTown = ftwns[i][1];
            tour += computeSqrt(h.get(fromTown), h.get(friendsTown));
            fromTown = friendsTown;
        }
        tour += h.get(fromTown);

        return Double.valueOf(Math.floor(tour)).intValue();
    }

    private static double computeSqrt(Double small, Double big) {
        return Math.sqrt(Math.pow(big, 2) - Math.pow(small, 2));
    }
}
