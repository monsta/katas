package kata.maximumSubarraySum;

import java.util.*;

/**
 * Created by johnson on 2016/6/21.
 */
public class Max {

    public static int sequence(int[] arr) {
        if (arr == null || arr.length == 0) {
            return 0;
        }

        boolean allNegative = true;
        for (int anArr : arr) {
            if (anArr > 0) {
                allNegative = false;
                break;
            }
        }

        if (allNegative) {
            return 0;
        } else {
            Set<Integer> sums = new TreeSet<>();
            for (int i = 0; i < arr.length - 1; i++) {
                for(int j = i+1; j < arr.length; j++) {
                    sums.add(sum(arr, i, j));
                }
            }
            Integer[] results = sums.toArray(new Integer[]{});
            return results[results.length-1];
        }
    }

    private static int sum(int[] arr, int start, int end) {
        int rtn = 0;
        for (int i = start; i <= end; i++) {
            rtn += arr[i];
        }
        return rtn;
    }
}
