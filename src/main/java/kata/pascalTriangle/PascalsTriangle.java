package kata.pascalTriangle;

/**
 * Created by johnson on 2016/5/27.
 */
public class PascalsTriangle {
    public static int[][] pascal(int i) {
        int[][] ret = new int[i][];

        for (int j = 0; j < ret.length; j++) {
            ret[j] = new int[j + 1];
            for (int k = 0; k < ret[j].length; k++) {
                ret[j][k] = (k == 0 || k ==j) ? 1 : (ret[j-1][k] + ret[j-1][k-1]);
            }
        }

        return ret;
    }
}
