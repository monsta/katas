package kata.pathInGrid;

import java.math.BigInteger;

/**
 * Created by johnson on 2016/7/1.
 */
public class GridPath {
    public static BigInteger numberOfRoutes(int m, int n) {
        return fact(m + n).divide(fact(m)).divide(fact(n));
    }

    private static BigInteger fact(int n) {
        if (n < 2) {
            return BigInteger.ONE;
        }
        return BigInteger.valueOf(n).multiply(fact(n - 1));
    }
}
