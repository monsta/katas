package kata.perimeterOfSquares;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by johnson on 2016/6/17.
 */
public class SumFct {

    public static BigInteger perimeter(BigInteger n) {
        BigInteger perimeter = BigInteger.ZERO;
        for (int i = 0; i <= n.intValue(); i++) {
            perimeter = perimeter.add(fib(BigInteger.valueOf(i)));
        }
        return BigInteger.valueOf(4).multiply(perimeter);
    }

    private static List<BigInteger> CACHE = new ArrayList<>();
    static {
        CACHE.add(BigInteger.ONE);
        CACHE.add(BigInteger.ONE);
    }
    private static BigInteger fib(BigInteger n) {
        if(n.intValue() >= CACHE.size()) {
            CACHE.add(n.intValue(), fib(n.subtract(BigInteger.ONE)).add(fib(n.subtract(BigInteger.valueOf(2)))));
        }
        return CACHE.get(n.intValue());
    }
}
