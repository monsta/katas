package kata.primeNumberDecomposition;

import java.util.*;

/**
 * Created by johnson on 2016/6/15.
 */
public class PrimeNumberDecomposer {

    public Long[] getAllPrimeFactors(long n) {
        if (n <= 0) {
            return new Long[]{};
        } else if (n == 1) {
            return new Long[]{1L};
        } else if (n == 2) {
            return new Long[]{2L};
        } else {
            List<Long> factors = primeFactors(n);
            return factors.toArray(new Long[factors.size()]);
        }
    }

    public Long[][] getUniquePrimeFactorsWithCount(long n) {
        if (n <= 0) {
            return new Long[][]{{},{}};
        } else if (n == 1) {
            return new Long[][]{{1L}, {1L}};
        } else if (n == 2) {
            return new Long[][]{{2L}, {1L}};
        } else {
            Map<Long, Long> factorsCount = getFactorsCountMap(n);

            Long[] factorsArray = factorsCount.keySet().toArray(new Long[]{});
            Long[] countArray = factorsCount.values().toArray(new Long[]{});

            return new Long[][]{factorsArray, countArray};
        }
    }

    public Long[] getPrimeFactorPotencies(long n) {
        if (n <= 0) {
            return new Long[]{};
        } else if (n == 1) {
            return new Long[]{1L};
        } else if (n == 2) {
            return new Long[]{2L};
        } else {
            Map<Long, Long> factorsCount = getFactorsCountMap(n);
            Iterator<Map.Entry<Long, Long>> it = factorsCount.entrySet().iterator();

            List<Long> rtn = new ArrayList<>();
            while (it.hasNext()) {
                Map.Entry<Long, Long> next = it.next();
                rtn.add(Double.valueOf(Math.pow(next.getKey(), next.getValue())).longValue());
            }
            return rtn.toArray(new Long[rtn.size()]);
        }
    }

    private Map<Long, Long> getFactorsCountMap(long n) {
        Long[] allPrimeFactors = getAllPrimeFactors(n);
        Map<Long, Long> factorsCount = new LinkedHashMap<>();

        for (Long factor : allPrimeFactors) {
            if (factorsCount.get(factor) == null) {
                factorsCount.put(factor, 1L);
            } else {
                factorsCount.put(factor, factorsCount.get(factor) + 1L);
            }
        }
        return factorsCount;
    }

    private List<Long> primeFactors(long number) {
        long n = number;
        List<Long> factors = new ArrayList<>();
        for (long i = 2; i <= n; i++) {
            while (n % i == 0) {
                factors.add(i);
                n /= i;
            }
        }
        return factors;
    }
}
