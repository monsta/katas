package kata.prizeDrawRank;

import java.util.Map;
import java.util.TreeMap;

/**
 * Created by johnson on 2016/7/28.
 */
public class Rank {

    private static final String STRING_RANK = "abcdefghijklmnopqrstuvwxyz";

    public static String nthRank(String st, Integer[] we, int n) {
        if (st == null || st.isEmpty()) {
            return "No participants";
        }

        String[] firstNames = st.split(",");
        if (n > firstNames.length) {
            return "Not enough participants";
        }

        Map<Integer, String> sortedFirstNames = new TreeMap<>();
        for (int i = 0; i < firstNames.length; i++) {
            String lowercaseFirstName = firstNames[i].toLowerCase();
            int realWinningNumber = getWinningNumber(lowercaseFirstName, we[i]);
            sortedFirstNames.put(realWinningNumber, firstNames[i]);
        }

        return (String) sortedFirstNames.values().toArray()[n-1];
    }

    private static int getWinningNumber(String lowercaseFirstName, int weight) {
        int n = lowercaseFirstName.length() + getRank(lowercaseFirstName);
        return weight * n;
    }

    private static int getRank(String lowercaseFirstName) {
        int rtn = 0;
        for (int i = 0; i < lowercaseFirstName.length(); i++) {
            rtn += getAlphabeticRank(lowercaseFirstName.charAt(i));
        }
        return rtn;
    }

    private static int getAlphabeticRank(char c) {
        return (int)c - 96;
    }
}
