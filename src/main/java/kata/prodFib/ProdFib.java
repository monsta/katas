package kata.prodFib;

/**
 * Created by johnson on 2016/8/11.
 */
public class ProdFib {

    public static long[] productFib(long prod) {
        long a = 0L;
        long b = 1L;
        while (a * b < prod) {
            long temp = a;
            a = b;
            b = temp + a;
        }
        return new long[] {a, b, a*b == prod ? 1 : 0};
    }
}
