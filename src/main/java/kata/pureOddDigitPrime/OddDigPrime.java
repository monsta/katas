package kata.pureOddDigitPrime;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by johnson on 2016/7/19.
 */
public class OddDigPrime {

    public static long[] onlyOddDigPrimes(long n) {
        long[] result = new long[3];
        List<Integer> allOddDigitPrimeBellow = allOddDigitPrimeBellow(n);
        result[0] = allOddDigitPrimeBellow.size();
        result[1] = allOddDigitPrimeBellow.get(allOddDigitPrimeBellow.size() - 1);
        result[2] = smallestOddDigitPrimeHigherThan(n, allOddDigitPrimeBellow.get(allOddDigitPrimeBellow.size() - 1));
        return result;
    }

    private static long smallestOddDigitPrimeHigherThan(long n, long maxOddDigitPrime) {
        return 0;
    }

    private static List<Integer> allOddDigitPrimeBellow(long upperBound) {
        int upperBoundSquareRoot = (int) Math.sqrt(upperBound);

        boolean[] isComposite = new boolean[(int) (upperBound + 1)];
        for (int m = 2; m <= upperBoundSquareRoot; m++) {
            if (!isComposite[m]) {
                //
                for (int k = m * m; k <= upperBound; k += m)
                    isComposite[k] = true;
            }
        }

        List<Integer> rtn = new ArrayList<>();
        for (int m = 2; m <= upperBound; m++)
            if (!isComposite[m])
                rtn.add(m);
        return rtn;
    }
}
