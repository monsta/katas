package kata.rectangleToSquare;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by johnson on 2016/7/18.
 */
public class SqInRect {

    public static List<Integer> sqInRect(int lng, int wdth) {
        //kata規則，第一次進來就丟相等的長寬，直接return null
        if (lng == wdth) {
            return null;
        } else {
            List<Integer> result = new ArrayList<>();
            return calculate(lng, wdth, result);
        }
    }

    private static List<Integer> calculate(int lng, int wdth, List<Integer> result) {
        if (lng == wdth) {
            result.add(lng);
        } else {
            int big;
            int little;
            if (lng < wdth) {
                big = wdth;
                little = lng;
            } else {
                big = lng;
                little = wdth;
            }
            if (big % little == 0) {
                for (int i = 0; i < big / little; i++) {
                    result.add(little);
                }
            } else {
                result.add(little);
                calculate(big - little, little, result);
            }
        }
        return result;
    }
}
