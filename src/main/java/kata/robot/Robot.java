package kata.robot;

import java.util.HashSet;
import java.util.Set;
import java.util.regex.Pattern;

/**
 * Created by johnson on 2016/6/29.
 */
public class Robot {
    private static Set<String> LEARNED = new HashSet<>();

    public String learnWord(String word) {
        String wordIn = word.toLowerCase();

        Pattern alphabeticCharacter = Pattern.compile("^[a-z]+$");
        if (!alphabeticCharacter.matcher(wordIn).matches()) {
            return "I do not understand the input";
        }

        if (LEARNED.contains(wordIn)) {
            return "I already know the word " + word;
        } else {
            LEARNED.add(wordIn);
            return "Thank you for teaching me " + word;
        }
    }
}
