package kata.scramblies;

/**
 * Created by johnson on 2016/6/8.
 */
public class Scramblies {
    public static boolean scramble(String str1, String str2) {
        if (str2.length() > str1.length()) {
            return false;
        }

        char[] str2Chars = str2.toCharArray();
        for (int i = 0; i < str2Chars.length; i++) {
            if (str1.indexOf(str2Chars[i]) < 0) {
                return false;
            } else {
                str1 = str1.replaceFirst(String.valueOf(str2Chars[i]), "");
            }

        }
        return true;
    }
}
