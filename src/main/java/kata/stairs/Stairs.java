package kata.stairs;

/**
 * Created by johnson on 2016/7/15.
 */
public class Stairs {
    public int NumberOfSteps(int stairs, int certainInt) {
        if(stairs < certainInt)
            return -1;
        if (stairs % 2 == 0) {
            return calculate(stairs / 2, 0, certainInt);
        } else {
            return calculate(stairs / 2, 1, certainInt);
        }
    }

    private int calculate(int stepsOf2, int stepsOf1, int certainInt) {
        if ((stepsOf2 + stepsOf1) % certainInt == 0) {
            return stepsOf2 + stepsOf1;
        } else {
            if (stepsOf2 - 1 <= 0) {
                return -1;
            }
            return calculate(stepsOf2 - 1, stepsOf1 + 2, certainInt);
        }
    }
}
