package kata.stockBroker;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by johnson on 2016/6/1.
 */
public class OrdersSummary {


    public static String balanceStatements(String lst) {

        String[] orderList = lst.split(",\\s");
        if (orderList.length > 0) {
            long buy = 0;
            long sell = 0;
            List<String> badOrders = new ArrayList<>();

            for (int i = 0; i < orderList.length; i++) {
                String order = orderList[i];
                if (!order.isEmpty()) {
                    String[] orderElements = order.split("\\s");

                    //檢查order規格
                    boolean bad = isOrderBadFormed(orderElements);
                    if (bad) {
                        badOrders.add(order);
                    } else {
                        if ("B".equals(orderElements[3])) {
                            buy += Math.round(Double.parseDouble(orderElements[1]) * Double.parseDouble(orderElements[2]));
                        } else if ("S".equals(orderElements[3])) {
                            sell += Math.round(Double.parseDouble(orderElements[1]) * Double.parseDouble(orderElements[2]));
                        }
                    }
                }
            }
            String buyAndSell = String.format("Buy: %s Sell: %s", String.valueOf(buy), String.valueOf(sell));

            if (!badOrders.isEmpty()) {
                StringBuffer result = new StringBuffer(buyAndSell)
                        .append("; Badly formed ").append(badOrders.size()).append(": ");
                for (int i = 0; i < badOrders.size(); i++) {
                    result.append(badOrders.get(i)).append(" ;");
                }
                return result.toString();
            }

            return buyAndSell;
        } else {
            return "";
        }
    }

    private static boolean isOrderBadFormed(String[] orderElements) {

        if (orderElements.length != 4) {
            return true;
        }
        try {
            Integer.parseInt(orderElements[1]);
        } catch (NumberFormatException e) {
            return true;
        }
        try {
            Double.parseDouble(orderElements[2]);

        } catch (NumberFormatException e) {
            return true;
        }
        if (!orderElements[2].contains(".")) {
            return true;
        }
        if (!"B".equals(orderElements[3]) && !"S".equals(orderElements[3])) {
            return true;
        }
        return false;
    }

}
