package kata.tenMinuteWalk;

/**
 * Created by johnson on 2016/6/4.
 */
public class TenMinWalk {

    public static boolean isValid(char[] walks) {
        int longitude = 0;
        int latitude = 0;

        if (walks.length != 10) {
            return false;
        }

        for (int i = 0; i < walks.length; i++) {
            switch (walks[i]) {
                case 'e':
                    longitude++;
                    continue;
                case 'w':
                    longitude--;
                    continue;
                case 's':
                    latitude--;
                    continue;
                case 'n':
                    latitude++;
                    continue;
                default:
                    throw new IllegalArgumentException("invalid walk command: " + walks[i]);
            }
        }
        return longitude == 0 && latitude == 0;
    }
}
