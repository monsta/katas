package kata.tortoise;

import java.math.BigDecimal;

/**
 * Created by johnson on 2016/6/2.
 */
public class Tortoise {

    private static double secondsPerHour = 3600;

    public static int[] race(int v1, int v2, int g) {
        if (v1 >= v2) {
            return null;
        }
        double feetPerSecondV1 = v1 / secondsPerHour;
        double feetPerSecondV2 = v2 / secondsPerHour;
        double secondsNeedV2CatchUpV1 = (double) g / (feetPerSecondV2 - feetPerSecondV1);
        return convertSecondsToResult(secondsNeedV2CatchUpV1);
    }

    private static int[] convertSecondsToResult(double seconds) {
        double secondsPerMinute = 60;
        int[] rtn = new int[3];

        rtn[0] = new BigDecimal(seconds).divide(new BigDecimal(secondsPerHour), BigDecimal.ROUND_HALF_UP).intValue();
        rtn[1] = new BigDecimal(seconds - rtn[0] * secondsPerHour).divide(new BigDecimal
                (secondsPerMinute), BigDecimal.ROUND_HALF_UP)
                .intValue();
        rtn[2] = new Long(Math.round(new BigDecimal(seconds - rtn[0] * secondsPerHour - rtn[1] * secondsPerMinute)
                .doubleValue())).intValue();
        return rtn;
    }
}
