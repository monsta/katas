package kata.clerk;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by johnson on 2016/5/26.
 */
public class LineTests {
    @Test
    public void test1() {
        assertEquals("YES", Line.tickets(new int[]{25, 25, 50}));
    }

    @Test
    public void testOneYes() {
        assertEquals("YES", Line.tickets(new int[]{25}));
    }

    @Test
    public void test3() {
        assertEquals("YES", Line.tickets(new int[]{25, 25, 25, 25}));
    }

    @Test
    public void test4() {
        assertEquals("YES", Line.tickets(new int[]{25, 50, 25, 50}));
    }

    @Test
    public void testOneNo() {
        assertEquals("NO", Line.tickets(new int[]{50}));
    }

    @Test
    public void test2() {
        assertEquals("NO", Line.tickets(new int[]{25, 100}));
    }

    @Test
    public void testALotAudience() {
        assertEquals("NO", Line.tickets(new int[]{25, 50, 25, 50, 25, 50, 100}));
    }

    @Test
    public void test5() {
        assertEquals("YES", Line.tickets(new int[]{25, 25, 25, 100}));
    }
}
