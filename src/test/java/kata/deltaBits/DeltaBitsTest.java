package kata.deltaBits;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class DeltaBitsTest {

    @Test
    public void test() throws Exception {
        assertThat(DeltaBits.convertBits(31, 14), is(2));
        assertThat(DeltaBits.convertBits(7, 17), is(3));
    }

    @Test
    public void testWithXORSolution() {
        assertThat(DeltaBitsXOR.convertBits(31, 14), is(2));
        assertThat(DeltaBitsXOR.convertBits(7, 17), is(3));
    }
}
