package kata.doubleChar;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestDoubleChar {
    @Test
    public void testDoubleString() {
        assertEquals("SSttrriinngg", DoubleChar.eval("String"));
        assertEquals("aabbccdd", DoubleChar.eval("abcd"));
        assertEquals("11223344!!__", DoubleChar.eval("1234!_"));
        assertEquals("!!@@##$$%%", DoubleChar.eval("!@#$%"));
    }
}
