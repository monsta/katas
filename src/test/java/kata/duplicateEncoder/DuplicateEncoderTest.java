package kata.duplicateEncoder;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by johnson on 2016/5/31.
 */
public class DuplicateEncoderTest {
    @Test
    public void test() {
        assertEquals("())((", DuplicateEncoder.encode("apple"));
        assertEquals(")()())()(()()(",
                DuplicateEncoder.encode("Prespecialized"));

        assertEquals("))))())))",DuplicateEncoder.encode("   ()(   "));
        assertEquals("()))())(()(()(", DuplicateEncoder.encode("Johnson Chuang"));
    }
}
