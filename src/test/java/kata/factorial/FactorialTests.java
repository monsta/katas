package kata.factorial;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by johnson on 2016/5/25.
 */
public class FactorialTests {

    private Factorial fact;

    @Before
    public void initFactorial() {
        fact = new Factorial();
    }

    @After
    public void afterFactorial() {
        fact = null;
    }

    @Test
    public void test_factorial0() {
        assertEquals(1, fact.factorial(0));
    }

    @Test
    public void test_factorial1() {
        assertEquals(1, fact.factorial(1));
    }

    @Test
    public void test_factorial3() {
        assertEquals(6, fact.factorial(3));
    }

    @Test
    public void test_factorial5() {
        assertEquals(120, fact.factorial(5));
    }

    @Test
    public void test_factorial12() {
        assertEquals(479001600, fact.factorial(12));
    }

    @Test(expected = IllegalArgumentException.class)
    public void test_factorial_lessThan0() {
        fact.factorial(-1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void test_factorial_moreThan12() {
        fact.factorial(13);
    }
}
