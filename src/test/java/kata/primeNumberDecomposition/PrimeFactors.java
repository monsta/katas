package kata.primeNumberDecomposition;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by johnson on 2016/6/15.
 */
public class PrimeFactors {
    public static List<Integer> primeFactors(int number) {
        int n = number;
        List<Integer> factors = new ArrayList<>();
        for (int i = 2; i <= n; i++) {
            while (n % i == 0) {
                factors.add(i);
                n /= i;
            }
        }
        return factors;
    }

    public static void main(String[] args) {
        List<Integer> factors = primeFactors(100);
        factors.forEach(fac -> System.out.println(fac));
    }
}
