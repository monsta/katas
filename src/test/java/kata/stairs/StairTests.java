package kata.stairs;

import org.junit.Test;
import static org.junit.Assert.assertEquals;
import org.junit.runners.JUnit4;


public class StairTests {
    @Test
    public void test1() {
        assertEquals(6, new Stairs().NumberOfSteps(10,2));
    }
    @Test
    public void test2() {
        assertEquals(-1, new Stairs().NumberOfSteps(3,5));
    }

    @Test
    public void test50() {
        System.out.println(new Stairs().NumberOfSteps(50, 3));
        System.out.println(new Stairs().NumberOfSteps(51, 3));
        System.out.println(new Stairs().NumberOfSteps(52, 3));
        System.out.println(new Stairs().NumberOfSteps(53, 3));
        System.out.println(new Stairs().NumberOfSteps(54, 3));
        System.out.println(new Stairs().NumberOfSteps(55, 3));
    }
}
