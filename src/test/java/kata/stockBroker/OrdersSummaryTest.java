package kata.stockBroker;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by johnson on 2016/6/1.
 */
public class OrdersSummaryTest {

    @Test
    public void test1() {
        String l = "ZNGA 1300 2.66 B, CLH15.NYM 50 56.32 B, OWW 1000 11.623 B, OGG 20 580.1 B";
        assertEquals("Buy: 29499 Sell: 0",
                OrdersSummary.balanceStatements(l));

        String t = "ZNGA 1300 2.66 B, CLH15.NYM 50 56.32 B";
        assertEquals("Buy: 6274 Sell: 0", OrdersSummary.balanceStatements(t));

        String c = "ZNGA 1300 2.66 B, CLH15.NYM 50 56.32 B, CLH16.NYM 50 56 S, OWW 1000 11 S";
        assertEquals("Buy: 6274 Sell: 0; Badly formed 2: CLH16.NYM 50 56 S ;OWW 1000 11 S ;",
                OrdersSummary.balanceStatements(c));

        String a = "";
        assertEquals("Buy: 0 Sell: 0", OrdersSummary.balanceStatements(a));
    }
}
