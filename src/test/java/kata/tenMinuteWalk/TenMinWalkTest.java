package kata.tenMinuteWalk;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by johnson on 2016/6/4.
 */
public class TenMinWalkTest {

    @Test
    public void test() {
        assertEquals("Should return true", true, TenMinWalk.isValid(new char[] {'n','s','n','s','n','s','n','s','n','s'}));
        assertEquals("Should return false", false, TenMinWalk.isValid(new char[] {'w','e','w','e','w','e','w','e','w','e','w','e'}));
        assertEquals("Should return false", false, TenMinWalk.isValid(new char[] {'w'}));
        assertEquals("Should return false", false, TenMinWalk.isValid(new char[] {'n','n','n','s','n','s','n','s','n','s'}));
        assertEquals("Should return true", true, TenMinWalk.isValid(new char[] {'e', 's', 'w', 'n', 'w', 'n', 'e',
                's', 'e', 'w'}));
    }
}
